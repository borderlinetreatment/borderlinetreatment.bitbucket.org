﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:html="http://www.w3.org/TR/REC-html40"
    xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>XML Video Sitemap</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style type="text/css">body {
          font-family:Georgia, serif;
          font-size:12px;
          background-color:#eee;
          width:1030px;
          margin:20px auto;
          }
          
          a {
          border:none;
          }
          
          a:link {
          color:#fff;
          text-decoration:none;
          }
          
          .labnol {
          position:relative;
          float:left;
          border:2px solid #000;
          margin:5px;
          height: 167px;
          background-color: black;
		  padding-top: 15px;
          }
          
          p {
          position: absolute;
          top: 106px;
          width: 93%;
          color: #fff;
          line-height: 18px;
          margin: 0;
          padding: 0 5px;
          text-shadow: 1px 1px 4px #000;
          font-weight: 800;
          right: 28px;
          font-family: arial;
          font-size: 13px;
          left: 2px;
          }
		  p:hover {
		  opacity: 0.9;
		  background-color: #2B2F45 !important;
		  border-radius: 3px;
		  box-shadow: 1px 1px 4px #000;
		  }
		</style>
      </head>
      <body>
        <xsl:for-each select="sitemap:urlset/sitemap:url">
          <xsl:variable name="u">
            <xsl:value-of select="sitemap:loc"/>
          </xsl:variable>
          <xsl:variable name="t">
            <xsl:value-of select="video:video/video:thumbnail_loc"/>
          </xsl:variable>
          <a href="{$u}" target="_blank">
            <div class="labnol">
              <img src="{$t}" width="240" height="150" />
              <p>
                <xsl:value-of select="video:video/video:title"/>
              </p>
            </div>
          </a>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
